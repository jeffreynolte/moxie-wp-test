<?php
 
 /**
 * The public-facing functionality of the plugin.
 *
 * @link       http://nalin.xyz
 * @since      1.0.0
 *
 * @package    Moxie_Wp_Test
 * @subpackage Moxie_Wp_Test/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Moxie_Wp_Test
 * @subpackage Moxie_Wp_Test/public
 * @author     Nalin Perera <gpnalin@gmail.com>
 */
class Moxie_WP_Test_Public {
    
    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;
    
    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $version ) {

        $this->version = $version;

    }
    
    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        // register bootstrap frontend framework
        wp_enqueue_style(
            'bootstrap',
            '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/css/bootstrap.min.css',
            array(),
            $this->version,
            FALSE
        );   

        // register Open Sans webfont
        wp_enqueue_style(
            'bootstrap',
            '//fonts.googleapis.com/css?family=Open+Sans',
            array(),
            $this->version,
            FALSE
        );       
        
        // registers plugin specific stylesheet to make any changes        
        wp_enqueue_style(
            'moxie-wp-test-public',
            plugin_dir_url( __FILE__ ) . 'css/moxie-wp-test-public.css',
            array('bootstrap'),
            $this->version,
            FALSE
        );
 
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        // registers the core angular file
        wp_enqueue_script(
            'angular',
            plugin_dir_url( __FILE__ ) . 'bower_components/angular/angular.min.js', 
            array(),
            $this->version, FALSE
        );

        // add angular routes module
        wp_enqueue_script(
            'angular-route',
            plugin_dir_url( __FILE__ ) . 'bower_components/angular-route/angular-route.min.js', 
            array(),
            $this->version, FALSE
        );

        // add angular animate module
        wp_enqueue_script(
            'angular-animate',
            plugin_dir_url( __FILE__ ) . 'bower_components/angular-animate/angular-animate.min.js', 
            array(),
            $this->version, FALSE
        );
        
        // add bootstrap specific scripts
        wp_enqueue_script(
            'bootstrap',
            '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.min.js', 
            array( 'jquery' ),
            $this->version, FALSE
        );       
        
        //add plugin specific script
        wp_enqueue_script( 
            'moxie-wp-test-public', 
            plugin_dir_url( __FILE__ ) . 'js/moxie-wp-test-public.js', 
            array( 'angular', 'angular-route', 'angular-animate', 'bootstrap' ),
            $this->version, FALSE
        );

        // localize few paths to use in scripts
        wp_localize_script(
            'moxie-wp-test-public',
            'myLocalized',
            array(
                'partials' => plugin_dir_url( __FILE__ ) . 'partials/'
            )
        );
    }

    /**
     * Set plugin specific homepage template.
     *
     * @since    1.0.0
     */
    function replace_home_page( $template ) {
      if ( is_front_page() ) {
        return plugin_dir_path( __FILE__ ) . 'templates/home_template.php';
      }
      return $template;
    }
 
}