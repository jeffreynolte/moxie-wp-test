<?php
/**
 * Class to register custom endpoint for the WP REST API
 *
 * Registers custom endpoint for movie custompost type.
 *
 * @package    Moxie_Wp_Test
 * @subpackage Moxie_Wp_Test/includes
 * @author     Nalin Perera <gpnalin@gmail.com>
 */

class Moxie_WP_Test_API_Movie extends WP_JSON_CustomPostType {

	/**
     * Path of the custom endpoint.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $base    base path.
     */
	protected $base = '/movies';

	/**
     * Custom post type slug.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $type    slug of the custom post type.
     */
	protected $type = 'movie';

	
	/**
     * Registers cutom endpoint.
     *
     * @since    1.0.0
     * @param    array   $routes    get the routes configurations.
     */
	public function register_routes( $routes ) {

		$routes = parent::register_routes( $routes );

		return $routes;

	}

}