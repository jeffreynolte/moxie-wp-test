<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://nalin.xyz
 * @since      1.0.0
 *
 * @package    Moxie_Wp_Test
 * @subpackage Moxie_Wp_Test/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Moxie_Wp_Test
 * @subpackage Moxie_Wp_Test/includes
 * @author     Nalin Perera <gpnalin@gmail.com>
 */
class Moxie_Wp_Test_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
